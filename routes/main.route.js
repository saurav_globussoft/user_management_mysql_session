const router = require("express").Router();
const session = require("express-session");

const mainController = require("../controller/main.controller")
const { checkIfLoggedIn, sessionChecker, ifSessionExist } = require("../middleware/auth");

router.use(session({
    key: "user_sid",
    secret: "secret123",
    resave: false,
    saveUninitialized: false,
    cookie: { expires: 5 * 24 * 60 * 60 * 1000 }
}));

router.use(checkIfLoggedIn);

router.get("/", sessionChecker, mainController.getHome);

router.route("/signup")
    .get(sessionChecker, mainController.getSignup)
    .post(mainController.createUserData);

router.route("/login")
    .get(sessionChecker, mainController.getLogin)
    .post(mainController.userLogin);

router.route("/update")
    .get( mainController.getUpdateUser)
    .post(mainController.updateUserData);

router.get("/forgetpass", mainController.forgetPassword);

router.get("/verify", mainController.verifyEmail);

router.route("/reset")
    .get(mainController.getReset)
    .post(mainController.resetPassword);

router.get("/dashboard", ifSessionExist, mainController.getDashboard);

router.get("/getUsers",ifSessionExist, mainController.getUserDetails)

router.get("/logout", mainController.logoutUser)

module.exports = { router };