const nodemailer = require("nodemailer");

/**
 * autoEmailLogin - function to send mail with credentials
 * @param {*} username 
 * @param {*} email 
 * @param {*} password 
 */
async function sendEmailLogin(username, email, password) {
    try {
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: process.env.EMAIL_ID,
                pass: process.env.PASS
            }
        });
        const mailOptions = {
            from: process.env.EMAIL_ID,
            to: email,
            subject: 'New user created',
            text: `Welcome your login credentials are :
                   Username : ${username} and Password : ${password}`
        };
        await transporter.sendMail(mailOptions);
    }
    catch (error) {
        console.log(error);
    }
}


/**
 * autoEmail - function to send email with generated token
 * @param {*} email 
 * @param {*} id 
 */
async function sendEmailVerify(email, id) {
    try {
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: process.env.EMAIL_ID,
                pass: process.env.PASS
            }
        });

        const mailOptions = {
            from: process.env.EMAIL_ID,
            to: email,
            subject: 'Email for user verification',
            text: `click on the given link to reset password: [ http://localhost:8000/reset?id=${id} ]`
        };

        await transporter.sendMail(mailOptions);
    }
    catch (error) {
        console.log(error);
    }
}

module.exports = { sendEmailLogin, sendEmailVerify };