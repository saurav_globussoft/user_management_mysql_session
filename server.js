const express = require("express");
const cookieParser = require("cookie-parser");

const app = express();

const dotenv = require('dotenv').config();

const PORT = process.env.PORT || 3000;

const { router } = require("./routes/main.route");

app.use(express.urlencoded({ extended: true }));

app.use(express.json());

app.use(cookieParser());

app.use(router);

app.set("view engine", "ejs");

app.listen(PORT, () => {
    console.log(`Listening to port : ${PORT}`);
});