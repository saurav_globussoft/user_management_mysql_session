function checkIfLoggedIn(req, res, next) {
    if (req.cookies.user_sid && !req.session.user) {
        res.clearCookie("user_sid");
        res.redirect("/login")
    }
    next();
}


function sessionChecker(req, res, next) {
    if (req.session.user && req.cookies.user_sid) {
        res.redirect("/dashboard");
    } else {
        next();
    }
}

function ifSessionExist(req, res, next) {
    if (req.session.user && req.cookies.user_sid) {
        next();
    } else {
        res.redirect("/login")
    }
}

module.exports = { checkIfLoggedIn, sessionChecker, ifSessionExist };