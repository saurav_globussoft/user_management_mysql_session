const mysql = require("../sql-test/sql.connection");

class Usermodel {

    /** 
    * checkUserExist - function to get username by matching username
    * @param {*} username 
    * @returns 
    */
    checkUserExist(username) {
        return new Promise((resolve, reject) => {
            if (!username) {
                reject("Error. Please enter username");
            }

            const query = `SELECT username FROM users
                           WHERE username = '${username}';`;
            mysql().query(query, (err, data) => {
                if (err) reject(err);
                resolve(data);
            });
        });
    }


    /**
     * checkUserExistById - function to get id by matching id
     * @param {*} id 
     * @returns 
     */
    checkUserExistById(id) {
        return new Promise((resolve, reject) => {
            const query = `SELECT id FROM users
                           WHERE id = ${id};`;
            mysql().query(query, (err, data) => {
                if (err) reject(err);
                resolve(data);
            });
        });
    }


    ifUserExist(id) {
        return new Promise((resolve, reject) => {
            const query = `SELECT username FROM users WHERE id = ${id};`;
            mysql().query(query, (err, data) => {
                if (err) reject(err);
                resolve(data);
            });
        });
    }


    /**
     * getUsersByUname - function to get id, username, name and password from database
     * @param {*} username 
     * @returns 
     */
    getUsersByUname(username) {
        const query = `SELECT id, username, name, password, email FROM users WHERE username = '${username}'`;
        return new Promise((resolve, reject) => {
            mysql().query(query, (err, data) => {
                if (err) reject(err);
                resolve(data);
            });
        });
    }


    /**
     * createUsers - function to create user
     * @param {*} username 
     * @param {*} name 
     * @param {*} password 
     * @returns 
     */
    createUsers(username, name, password, email) {
        return new Promise((resolve, reject) => {

            const query = `INSERT INTO users (username, name, password, email)
                        VALUES ('${username}', '${name}', '${password}', '${email}');`;
            mysql().query(query, (err, data) => {
                if (err) reject(err);
                resolve(data);
            });
        });
    }

    /**
     * getUsers - function to get username and name from database
     * @returns 
     */
    getUsers() {
        const query = `SELECT username, name FROM users`;
        return new Promise((resolve, reject) => {
            mysql().query(query, (err, data) => {
                if (err) reject(err);
                resolve(data);
            });
        });
    }


    /**
         * updateUsers - function to update name, email and password
         * @param {*} username 
         * @param {*} name 
         * @param {*} password 
         * @returns 
         */
    updateUsers(username, name, password, email) {
        return new Promise((resolve, reject) => {
            let updateStr = ``;
            if (name) {
                updateStr += updateStr ? ` , name = '${name}' ` : ` SET name = '${name}' `;
            }

            if (password) {
                updateStr += updateStr ? ` , password = '${password}' ` : ` SET password = '${password}' `;
            }

            if (email) {
                updateStr += updateStr ? ` , email = '${email}' ` : ` SET email = '${email}' `;
            }

            const query = `UPDATE users ${updateStr}
                            WHERE username = '${username}';`;

            mysql().query(query, (err, data) => {
                if (err) reject(err);
                resolve(data);
            });
        });
    }


    /**
     * resetPassDb - function to reset password
     * @param {*} id 
     * @param {*} password 
     * @returns 
     */
    resetPassDb(id, password) {
        return new Promise((resolve, reject) => {
            const query = `UPDATE users SET password = '${password}' 
                            WHERE id = '${id}';`;
            mysql().query(query, (err, data) => {
                if (err) reject(err);
                resolve("Password changed");
            });
        });
    }
}

module.exports = new Usermodel;