const mysql = require('mysql');

/**
 * mysqlConnection - function to create connection with mySql
 * @returns 
 */
function mysqlConnection() {
    const connection = mysql.createConnection({
        host: '127.0.0.1',
        user: 'root',
        password: '',
        database: 'user_management_session'
    });

    connection.connect(function (err) {
        if (err) {
            console.error('error connecting: ' + err.stack);
            return;
        }

        console.log('connected as id ' + connection.threadId);
    });
    return connection;
}

module.exports = mysqlConnection;