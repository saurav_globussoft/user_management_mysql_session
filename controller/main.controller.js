const userModel = require("../model/user.model")
const encryptDecrypt = require("../services/encryptPass.services")
const { sendEmailLogin, sendEmailVerify } = require("../services/email.services");

class MainController {

    getHome(req, res) {
        res.render("index", { title: "WELCOME TO HOMEPAGE" });
    }

    getSignup(req, res) {
        res.render("signup", { title: "CREATE YOUR ACCOUNT" });
    }

    getLogin(req, res) {
        res.render("login", { title: "LOGIN IN HERE" });
    }

    getUpdateUser(req, res) {
        res.render("update", { title: "UPDATE" });
    }

    forgetPassword(req, res) {
        res.render("forgetpass", { title: "CHANGE PASSWORD" });

    }

    getReset(req, res) {
        console.log(req.query);
        res.render("changepass", {
            title: "RESET PASSWORD",
            id: req.query.id
        });
    }

    logoutUser(req, res) {
        if (req.session.user && req.cookies.user_sid) {
            res.clearCookie("user_sid");
            res.redirect("/");
        } else {
            res.redirect("/login");
        }
    }

    getDashboard(req, res) {
        if (req.session.user && req.cookies.user_sid) {
            res.render("dasboard", { title: "DASHBOARD" });
        } else {
            res.redirect("/login")
        }
    }
    async createUserData(req, res) {
        try {
            const { username, password, name, email } = req.body;

            if ((!username) || (!password) || (!name) || (!email)) {
                return res.redirect("/signup");
            }

            const [checkUserExist] = await userModel.checkUserExist(username);
            if (checkUserExist) return res.status(405).json({ code: 405, message: "User Already exist" });

            const pass = await encryptDecrypt.encryptPass(password);
            const createUserData = await userModel.createUsers(username, name, pass, email);

            const [userData] = await userModel.getUsersByUname(username);

            req.session.user = userData.id;

            const aEmail = await sendEmailLogin(username, email, password);

            return res.status(201).redirect("/dashboard");
        }
        catch (error) {
            res.status(404).json(error);
        }
    }



    /**
     * updateUserData - function to update user
     * @param {*} req 
     * @param {*} res 
     * @returns 
     */
    async updateUserData(req, res) {
        try {
            const { username, name, password, email } = req.body;
            const id = req.session.user;
            const [ifUserExist] = await userModel.ifUserExist(id);
            // console.log();

            // const [checkUserExist] = await userModel.checkUserExist(username);
            if (ifUserExist.username !== username) {
                return res.render("template", {
                    title: "USERNAME NOT FOUND",
                    message: JSON.stringify({ message: "Username doesnot exist. Please enter correct username" })
                });
            }

            const pass = await encryptDecrypt.encryptPass(password);

            const updateUserData = await userModel.updateUsers(username, name, pass, email);
            return res.render("template", {
                title: "USER UPDATED",
                message: JSON.stringify({ message: "Your details updated" })
            })
        }
        catch (error) {
            res.status(404).json(error);
        }
    }



    /**
     * userLogin - function to login user 
     * @param {*} req 
     * @param {*} res 
     * @returns 
     */
    async userLogin(req, res) {
        try {
            const { username, password } = req.body;

            const [checkUserExist] = await userModel.checkUserExist(username);
            if (!checkUserExist) {
                return res.render("template", {
                    title: "USERNAME NOT FOUND",
                    message: JSON.stringify({ message: "Username doesnot exist. Please enter correct username" })
                });
            }

            const [userData] = await userModel.getUsersByUname(username);
            const decrptPass = await encryptDecrypt.decryptPass(password, userData.password);

            req.session.user = userData.id;

            res.redirect("/dashboard");
        } catch (error) {
            res.render("template", {
                title: "PASSWORD INCORRECT",
                message: JSON.stringify({ message: "Password incorrect" })
            });
        }
    }


    /**
   * verifyEmail - funtion to very username and email
   * @param {*} req 
   * @param {*} res 
   * @returns 
   */
    async verifyEmail(req, res) {
        try {
            const { username } = req.query;
            const [checkUserExist] = await userModel.checkUserExist(username);
            if (!checkUserExist) {
                return res.render("template", {
                    title: "USER DETAILS",
                    message: JSON.stringify({ message: "Username Incorrect" })
                });
            }
            const [userData] = await userModel.getUsersByUname(username);

            const aEmail = await sendEmailVerify(userData.email, userData.id);

            res.render("template", {
                title: "MAIL SENT",
                message: "Verification mail sent to your registerd email"
            });
        }
        catch (error) {
            return res.status(404).json(error);
        }
    }

    async getUserDetails(req, res) {
        const userDataDB = await userModel.getUsers();
        res.render("template", {
            title: "USER DETAILS",
            message: JSON.stringify(userDataDB)
        })
    }
    /**
         * resetPassword - function to reset password
         * @param {*} req 
         * @param {*} res 
         * @returns 
         */
    async resetPassword(req, res) {
        try {
            const { password1, password2 } = req.body;
            const { id } = req.query;
            if (!id) {
                return res.render("template", {
                    title: "ID NOT FOUND",
                    message: JSON.stringify({ message: "Please enter id to change password" })
                });
            }

            if ((!password1) || (!password2)) {
                return res.render("template", {
                    title: "PASSWORD NOT ENTERED",
                    message: JSON.stringify({ message: "Password not entered, New Password and Confirm password should not be empty" })
                });
            }

            if (password1 !== password2) {
                return res.render("template", {
                    title: "PASSWORD MISSMATCH",
                    message: JSON.stringify({ message: "Password mismatched, New Password and Confirm password should be same" })
                });
            }

            const [checkUserExist] = await userModel.checkUserExistById(id);

            if (!checkUserExist) {
                return res.render("template", {
                    title: "NOT FOUND",
                    message: JSON.stringify({ message: "ID entered was wrong" })
                });
            }

            const pass = await encryptDecrypt.encryptPass(password1);
            const resetPassDb = await userModel.resetPassDb(id, pass);

            return res.render("template", {
                title: "PASSWORD CHANGED SUCCESSFULLY",
                message: JSON.stringify({ message: "Password changed successfully" })
            });

        } catch (error) {
            return res.status(400).json(error);
        }
    }

}

module.exports = new MainController;